export const PREFIX = process.env.PREFIX ?? '/api/v1';
export const POSTGRES_HOST = process.env.POSTGRES_HOST ?? '127.0.0.1';
export const POSTGRES_PORT = process.env.POSTGRES_PORT ? +process.env.POSTGRES_PORT : 5432;
export const POSTGRES_USER = process.env.POSTGRES_USER ?? 'root';
export const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD ?? 'root';
export const POSTGRES_DATABASE = process.env.POSTGRES_DATABASE ?? 'store';
export const PORT = process.env.PORT ? +process.env.PORT : 8080;
