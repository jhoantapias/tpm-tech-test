export interface InputNewProductModel {
    description: string;
    name: string;
    price: number;
}
