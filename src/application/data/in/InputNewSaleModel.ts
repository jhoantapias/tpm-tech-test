export interface InputNewSaleModel {
    products_id: string;
    qty: number;
    sales_at: string;
    users_id: string;
}
