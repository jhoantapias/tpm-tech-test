export interface InputNewUserModel {
    document: string;
    last_name: string;
    name: string;
    roles_id: string;
}
