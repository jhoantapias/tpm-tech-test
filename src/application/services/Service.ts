import { DEPENDENCY_CONTAINER, TYPES } from '@configuration';
import { IQueryUpdate, ListRepository, UpdateRepository } from '@domain/repositories';
import { DeleteEntityRepository } from '@domain/repositories/DeleteEntityRepository';
import { NewEntityRepository } from '@domain/repositories/NewEntityRepository';
import { Response } from '@domain/response';
import { injectable } from 'inversify';

export type Res<T> = Promise<Response<T | null>>;

@injectable()
export class RegisterService {
    public static get repository(): NewEntityRepository {
        return DEPENDENCY_CONTAINER.get<NewEntityRepository>(TYPES.NewEntityRepository);
    }
}

@injectable()
export class ListService {
    public static get repository(): ListRepository {
        return DEPENDENCY_CONTAINER.get<ListRepository>(TYPES.ListRepository);
    }
}

@injectable()
export class UpdaterService {
    public static get repository(): UpdateRepository {
        return DEPENDENCY_CONTAINER.get<UpdateRepository>(TYPES.UpdateRepository);
    }
}

@injectable()
export class DeleteService {
    public static get repository(): DeleteEntityRepository {
        return DEPENDENCY_CONTAINER.get<DeleteEntityRepository>(TYPES.DeleteEntityRepository);
    }
}

export interface Register<Model, Entity> {
    recordNew(data: Model): Res<Entity>;
}
export interface List<Entity> {
    getList(): Res<Entity[]>;
}

export interface Updater<Model> {
    update(data: Model, id: string): Res<null>;
    query(data: Record<string, string | number>, id: string): IQueryUpdate;
}

export interface Deleter {
    delete(id: string): Res<null>;
}
