import { DEPENDENCY_CONTAINER, TYPES } from '@configuration';
import { LastMonthResumeRepository } from '@domain/repositories';
import { Result } from '@domain/response';
import { injectable } from 'inversify';
import { Res } from '../Service';

interface ITotal {
    total: number;
}

@injectable()
export class LastMonthResumeService {
    private repository = DEPENDENCY_CONTAINER.get<LastMonthResumeRepository>(TYPES.LastMonthResumeRepository);
    async report(): Res<ITotal> {
        const total = await this.repository.report();
        return Result.ok({ total });
    }
}
