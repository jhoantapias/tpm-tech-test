import { SaleEntity } from '@domain/contexts';
import { Result } from '@domain/response';
import { injectable } from 'inversify';
import { List, ListService, Res } from '../Service';

@injectable()
export class SalesListService implements List<SaleEntity> {
    async getList(): Res<SaleEntity[]> {
        const list = await ListService.repository.get<SaleEntity>('public.sales');
        return Result.ok(list);
    }
}
