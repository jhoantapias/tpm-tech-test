import { SaleEntity } from '@domain/contexts';
import { SaleModel } from '@domain/contexts';
import { Result } from '@domain/response';
import { injectable } from 'inversify';
import { Register, RegisterService, Res } from '../Service';

@injectable()
export class SaleRegisterService implements Register<SaleModel, SaleEntity> {
    async recordNew(data: SaleModel): Res<SaleEntity> {
        const entity = new SaleEntity(data);
        await RegisterService.repository.create(entity, 'public.sales');
        return Result.ok(entity);
    }
}
