import { ProductEntity } from '@domain/contexts';
import { Result } from '@domain/response';
import { injectable } from 'inversify';
import { List, ListService, Res } from '../Service';

@injectable()
export class ProductListService implements List<ProductEntity> {
    async getList(): Res<ProductEntity[]> {
        const list = await ListService.repository.get<ProductEntity>('public.products');
        return Result.ok(list);
    }
}
