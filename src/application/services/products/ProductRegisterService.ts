import { InputNewProductModel } from '@application/data';
import { ProductEntity, ProductModel } from '@domain/contexts';
import { Response, Result } from '@domain/response';
import { injectable } from 'inversify';
import { Register, RegisterService } from '../Service';

type Res = Response<ProductEntity | null>;

@injectable()
export class ProductRegisterService implements Register<ProductModel, ProductEntity> {
    public async recordNew(data: InputNewProductModel): Promise<Res> {
        const entity = new ProductEntity(data);
        await RegisterService.repository.create(entity, 'public.products');
        return Result.ok(entity);
    }
}
