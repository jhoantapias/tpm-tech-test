import { UserEntity } from '@domain/contexts';
import { UserModel } from '@domain/contexts';
import { Result } from '@domain/response';
import { injectable } from 'inversify';
import { Register, RegisterService, Res } from '../Service';

@injectable()
export class UserRegisterService implements Register<UserModel, UserEntity> {
    async recordNew(data: UserModel): Res<UserEntity> {
        const entity = new UserEntity(data);
        await RegisterService.repository.create(entity, 'public.users');
        return Result.ok(entity);
    }
}
