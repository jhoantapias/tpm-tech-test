import { UserEntity } from '@domain/contexts';
import { Result } from '@domain/response';
import { injectable } from 'inversify';
import { List, ListService, Res } from '../Service';

@injectable()
export class UsersListService implements List<UserEntity> {
    async getList(): Res<UserEntity[]> {
        const list = await ListService.repository.get<UserEntity>('public.users');
        return Result.ok(list);
    }
}
