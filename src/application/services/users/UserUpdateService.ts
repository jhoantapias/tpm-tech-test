import { UserModel } from '@domain/contexts';
import { IQueryUpdate } from '@domain/repositories';
import { Result } from '@domain/response';
import { injectable } from 'inversify';
import { Res, Updater, UpdaterService } from '../Service';

@injectable()
export class UserUpdateService implements Updater<UserModel> {
    async update(data: UserModel, user_id: string): Res<null> {
        const info = this.query(data, user_id);
        await UpdaterService.repository.update(info);
        return Result.ok();
    }
    query(data: Record<string, string | number>, id: string): IQueryUpdate {
        return {
            data,
            table: 'public.users',
            where_field: 'id',
            where_value: id,
        };
    }
}
