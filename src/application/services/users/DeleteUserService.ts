import { Result } from '@domain/response';
import { injectable } from 'inversify';
import { Deleter, DeleteService, Res } from '../Service';

@injectable()
export class DeleteUserService implements Deleter {
    async delete(id: string): Res<null> {
        await DeleteService.repository.delete(id, 'public.users');
        return Result.ok();
    }
}
