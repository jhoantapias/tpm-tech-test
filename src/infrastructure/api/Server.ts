import dotenv from 'dotenv';
import 'module-alias/register';
dotenv.config();
import { application } from './Application';
import { DEPENDENCY_CONTAINER, initDependencyContainer } from '@configuration';
import { PORT } from '@util';
import { PostgresMigrationsRepository } from '@infrastructure/repositories';
import path from 'path';
import { existsSync } from 'fs';

const start = async () => {
    const file = path.join(__dirname, '..', '..', '..', 'migrations', 'migrate.sql');
    try {
        const server = await application.listen({ port: PORT, host: '0.0.0.0' });
        initDependencyContainer();
        if (existsSync(file)) {
            const migrations = DEPENDENCY_CONTAINER.get(PostgresMigrationsRepository);
            await migrations.run(file);
        }
        console.log(`Application running on ${server}`);
    } catch (error) {
        console.error(error);
        await application.close();
    }
};
start();
