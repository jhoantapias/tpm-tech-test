import {
    deleteSaleRouter,
    deleteUserRouter,
    listProductsRouter,
    listSalesRouter,
    listUsersRouter,
    newProductRouter,
    newSaleRouter,
    newUserRouter,
    reportSalesRouter,
    updateSaleRouter,
    updateUsersRouter,
} from './router';
import { FastifyInstance } from 'fastify';
import { authMiddleware } from '@infrastructure/api/middlewares/AuthMiddleware';

export const initRoutes = async (application: FastifyInstance): Promise<void> => {
    application.post(
        `/product/new`,
        {
            preValidation: authMiddleware,
        },
        newProductRouter,
    ); //NOTE: Registrar nuevo usuario
    application.get(
        '/product/list',
        {
            preValidation: authMiddleware,
        },
        listProductsRouter,
    ); //NOTE: Obtener lista de productos
    application.post(
        '/sales/new',
        {
            preValidation: authMiddleware,
        },
        newSaleRouter,
    ); //NOTE: Registrar nueva venta
    application.post(
        '/users/new',
        {
            preValidation: authMiddleware,
        },
        newUserRouter,
    ); //NOTE: Crear usuario
    application.get(
        '/users/list',
        {
            preValidation: authMiddleware,
        },
        listUsersRouter,
    ); //NOTE: Lista de usuarios
    application.put(
        '/users/update/:id',
        {
            preValidation: authMiddleware,
        },
        updateUsersRouter,
    ); //NOTE: Actualizar usuario
    application.get(
        '/sales/list',
        {
            preValidation: authMiddleware,
        },
        listSalesRouter,
    ); //NOTE: Lista de ventas
    application.put(
        '/sales/update/:id',
        {
            preValidation: authMiddleware,
        },
        updateSaleRouter,
    ); //NOTE: Actualizar venta
    application.delete(
        '/sales/delete/:id',
        {
            preValidation: authMiddleware,
        },
        deleteSaleRouter,
    ); //NOTE: Eliminar venta
    application.delete(
        '/users/delete/:id',
        {
            preValidation: authMiddleware,
        },
        deleteUserRouter,
    ); //NOTE: Eliminar usuario
    application.get(
        '/sales/report-last-month/',
        {
            preValidation: authMiddleware,
        },
        reportSalesRouter,
    ); //NOTE: Obtener la suma total del las ventas del ultimo mes
};
