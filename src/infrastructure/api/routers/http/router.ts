import { InputNewProductModel } from '@application/data';
import { ProductListService } from '@application/services/products/ProductListService';
import { ProductRegisterService } from '@application/services/products/ProductRegisterService';
import { DeleteSaleService } from '@application/services/sales';
import { LastMonthResumeService } from '@application/services/sales/LastMonthResumeService';
import { SaleRegisterService } from '@application/services/sales/SaleRegisterService';
import { SalesListService } from '@application/services/sales/SalesListService';
import { SaleUpdateService } from '@application/services/sales/SaleUpdateService';
import { UserRegisterService, UsersListService } from '@application/services/users';
import { DeleteUserService } from '@application/services/users/DeleteUserService';
import { UserUpdateService } from '@application/services/users/UserUpdateService';
import { DEPENDENCY_CONTAINER } from '@configuration';
import { SaleModel } from '@domain/contexts/sales/models';
import { UserModel } from '@domain/contexts/users/models';
import { newProductSchema, newSaleSchema, newUserSchema, idSchema } from '@infrastructure/api';
import { validateData } from '@infrastructure/api/util';
import { FastifyRequest, FastifyReply } from 'fastify';

export const newProductRouter = async (req: FastifyRequest, reply: FastifyReply): Promise<FastifyReply | void> => {
    const data = validateData<InputNewProductModel>(newProductSchema, req.body);
    const service = DEPENDENCY_CONTAINER.get(ProductRegisterService);
    const response = await service.recordNew(data);
    return reply.send(response);
};

export const listProductsRouter = async (_req: FastifyRequest, reply: FastifyReply): Promise<FastifyReply | void> => {
    const service = DEPENDENCY_CONTAINER.get(ProductListService);
    const list = await service.getList();
    return reply.send(list);
};

export const newSaleRouter = async (req: FastifyRequest, reply: FastifyReply): Promise<FastifyReply | void> => {
    const data = validateData<SaleModel>(newSaleSchema, req.body);
    const service = DEPENDENCY_CONTAINER.get(SaleRegisterService);
    const response = await service.recordNew(data);
    return reply.send(response);
};
export const listSalesRouter = async (_req: FastifyRequest, reply: FastifyReply): Promise<FastifyReply | void> => {
    const service = DEPENDENCY_CONTAINER.get(SalesListService);
    const list = await service.getList();
    return reply.send(list);
};
export const updateSaleRouter = async (req: FastifyRequest, reply: FastifyReply): Promise<FastifyReply> => {
    const data = validateData<SaleModel>(newSaleSchema, req.body);
    const { id } = validateData<{ id: string }>(idSchema, req.params);
    const service = DEPENDENCY_CONTAINER.get(SaleUpdateService);
    const response = await service.update(data, id);
    return reply.send(response);
};

export const deleteSaleRouter = async (req: FastifyRequest, reply: FastifyReply): Promise<FastifyReply> => {
    const { id } = validateData<{ id: string }>(idSchema, req.params);
    const service = DEPENDENCY_CONTAINER.get(DeleteSaleService);
    const response = await service.delete(id);
    return reply.send(response);
};

export const reportSalesRouter = async (_req: FastifyRequest, reply: FastifyReply): Promise<FastifyReply> => {
    const service = DEPENDENCY_CONTAINER.get(LastMonthResumeService);
    const response = await service.report();
    return reply.send(response);
};
export const newUserRouter = async (req: FastifyRequest, reply: FastifyReply): Promise<FastifyReply | void> => {
    const data = validateData<UserModel>(newUserSchema, req.body);
    const service = DEPENDENCY_CONTAINER.get(UserRegisterService);
    const response = await service.recordNew(data);
    return reply.send(response);
};
export const deleteUserRouter = async (req: FastifyRequest, reply: FastifyReply): Promise<FastifyReply> => {
    const { id } = validateData<{ id: string }>(idSchema, req.params);
    const service = DEPENDENCY_CONTAINER.get(DeleteUserService);
    const response = await service.delete(id);
    return reply.send(response);
};
export const listUsersRouter = async (_req: FastifyRequest, reply: FastifyReply): Promise<FastifyReply> => {
    const service = DEPENDENCY_CONTAINER.get(UsersListService);
    const list = await service.getList();
    return reply.send(list);
};

export const updateUsersRouter = async (req: FastifyRequest, reply: FastifyReply): Promise<FastifyReply> => {
    const data = validateData<UserModel>(newUserSchema, req.body);
    const { id } = validateData<{ id: string }>(idSchema, req.params);
    const service = DEPENDENCY_CONTAINER.get(UserUpdateService);
    const response = await service.update(data, id);
    return reply.send(response);
};
