import Joi from 'joi';
import { BadMessageException } from '@domain/exceptions';
import { ERROR_MESSAGES } from '@domain/exceptions/messages';

type Schema = Joi.ObjectSchema | Joi.ArraySchema;
type Body = Record<string, unknown> | undefined | unknown;

export const validateData = <T>(schema: Schema, dataToValidate: Body): T => {
    if (dataToValidate) {
        const { error, value } = schema.validate(dataToValidate, { convert: true });
        if (error) {
            console.error(`schemaError: ${JSON.stringify(error)}`);
            throw new BadMessageException(error.message, ERROR_MESSAGES.ERR_INPUT_DATA);
        }
        return value;
    }
    throw new BadMessageException(ERROR_MESSAGES.NO_DATA_TO_VALIDATE);
};
