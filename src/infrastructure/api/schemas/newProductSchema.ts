import { InputNewProductModel } from '@application/data';
import Joi from 'joi';

export const newProductSchema = Joi.object<InputNewProductModel>({
    description: Joi.string().optional().allow('').default(''),
    name: Joi.string().required(),
    price: Joi.number().required(),
});
