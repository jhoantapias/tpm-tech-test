import Joi from 'joi';
import { AuthModel } from '../middlewares/AuthMiddleware';

export const authSchema = Joi.object<AuthModel>({
    auth: Joi.string().required().messages({
        'any.required': 'No existe el id del usuario en los headers',
    }),
}).unknown(true);
