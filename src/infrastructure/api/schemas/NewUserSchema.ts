import { InputNewUserModel } from '@application/data';
import Joi from 'joi';

export const newUserSchema = Joi.object<InputNewUserModel>({
    document: Joi.string().required(),
    last_name: Joi.string().required(),
    name: Joi.string().required(),
    roles_id: Joi.string().required(),
});
