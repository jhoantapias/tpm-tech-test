import { InputNewSaleModel } from '@application/data';
import Joi from 'joi';

export const newSaleSchema = Joi.object<InputNewSaleModel>({
    products_id: Joi.string().required(),
    qty: Joi.number().required(),
    sales_at: Joi.string().required(),
    users_id: Joi.string().required(),
});
