import Joi from 'joi';

export const idSchema = Joi.object<{ id: string }>({
    id: Joi.string().required(),
});
