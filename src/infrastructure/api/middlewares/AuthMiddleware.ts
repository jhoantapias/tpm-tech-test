import { DEPENDENCY_CONTAINER } from '@configuration';
import { ADMIN, EVERYONE } from '@domain/auth/Routes';
import { AuthException } from '@domain/exceptions';
import { PgRoleRepository } from '@infrastructure/repositories/postgresql/dao/PgRoleRepository';
import { FastifyReply, FastifyRequest, HookHandlerDoneFunction } from 'fastify';
import { authSchema } from '../schemas/authSchema';
import { validateData } from '../util';
export interface AuthModel {
    auth: string;
}
export interface IRole {
    id: string;
    name: 'admin' | 'employee';
}
export const authMiddleware = async (req: FastifyRequest, _reply: FastifyReply, done: HookHandlerDoneFunction) => {
    const { auth } = validateData<AuthModel>(authSchema, req.headers);
    const roleRepo = DEPENDENCY_CONTAINER.get(PgRoleRepository);
    const role = await roleRepo.get(auth);
    if (!role || !role.id || !role.name) {
        return done(new AuthException());
    }

    if (role.name === 'admin') {
        const hasAccess = ADMIN.find((url) => req.url.includes(url));
        if (hasAccess) return done();
    }
    if (role.name === 'employee') {
        const hasAccess = EVERYONE.find((url) => req.url.includes(url));
        if (hasAccess) return done();
    }

    done(new AuthException());
};
