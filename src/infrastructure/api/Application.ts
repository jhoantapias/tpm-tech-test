// libraries
import 'reflect-metadata';
import 'module-alias/register';
import dotenv from 'dotenv';
dotenv.config();
import fastify from 'fastify';
import { randomBytes } from 'crypto';
import { PREFIX } from '@util';
import { initRoutes, middlewares, errorHandler } from '@infrastructure/api';

export const application = fastify({
    genReqId: (_) => randomBytes(20).toString('hex'),
});

application.addHook('onRoute', (r) => {
    console.log(r.method, r.url);
});

// middlewares
middlewares(application);
errorHandler(application);

// routes
application.register(initRoutes, { prefix: PREFIX });
