import { IQueryUpdate } from '@domain/repositories';

export const getUpdateSentence = ({ table, data, where_field, where_value }: IQueryUpdate): string => {
    let sentence = `UPDATE ${table} SET`;
    const entries = Object.entries(data);
    entries.forEach((e, i, a) => {
        const [key, v] = e;
        const coma = i === a.length - 1 ? '' : ',';
        sentence += ` ${key} = ${typeof v === 'number' ? v : `'${v}'`}${coma} `;
    });

    sentence += ` WHERE ${where_field} = '${where_value}'`;
    return sentence;
};
