import { DEPENDENCY_CONTAINER, TYPES } from '@configuration';
import { PostgresError } from '@domain/exceptions';
import { IRole } from '@infrastructure/api/middlewares/AuthMiddleware';

import { injectable } from 'inversify';
import { PgDatabase } from '../adapter';

@injectable()
export class PgRoleRepository {
    private db = DEPENDENCY_CONTAINER.get<PgDatabase>(TYPES.Postgres);

    async get(id: string): Promise<IRole | null> {
        try {
            const query =
                'select r.id, r."name" from public.roles r inner join public.users u on u.roles_id = r.id where u.id = $1;';
            const role = await this.db.oneOrNone<IRole>(query, [id]);
            return role;
        } catch (error) {
            throw new PostgresError(error.code, error.message);
        }
    }
}
