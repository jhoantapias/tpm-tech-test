import { DEPENDENCY_CONTAINER, TYPES } from '@configuration';
import { PostgresError } from '@domain/exceptions';
import { DeleteEntityRepository } from '@domain/repositories/DeleteEntityRepository';
import { injectable } from 'inversify';
import { PgDatabase } from '../adapter';

@injectable()
export class PgDeleteEntityRepository implements DeleteEntityRepository {
    private db = DEPENDENCY_CONTAINER.get<PgDatabase>(TYPES.Postgres);
    async delete(id: string, table: string): Promise<void> {
        try {
            const query = `DELETE FROM ${table} WHERE id = $1`;
            await this.db.none(query, [id]);
        } catch (error) {
            throw new PostgresError(error.code, error.message);
        }
    }
}
