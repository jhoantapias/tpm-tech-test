import { DEPENDENCY_CONTAINER, TYPES } from '@configuration';
import { PostgresError } from '@domain/exceptions';
import { LastMonthResumeRepository } from '@domain/repositories/LastMonthResumeRepository';
import { injectable } from 'inversify';
import { PgDatabase } from '../adapter';

interface IPrices {
    price: number;
}

@injectable()
export class PgLastMonthResumeRepository implements LastMonthResumeRepository {
    private db = DEPENDENCY_CONTAINER.get<PgDatabase>(TYPES.Postgres);

    public async report(): Promise<number> {
        try {
            const query = `SELECT p.price FROM public.sales s
                            INNER JOIN public.products p ON s.products_id = p.id
                            WHERE sales_at > CURRENT_DATE - INTERVAL '1 months'`;
            const result = await this.db.manyOrNone<IPrices>(query);
            const values = this.getValues(result);
            return this.getTotal(values);
        } catch (error) {
            throw new PostgresError(error.code, error.message);
        }
    }
    private getTotal(arr: number[]): number {
        const reducer = (accumulator: number, curr: number) => accumulator + curr;
        return arr.reduce(reducer);
    }
    private getValues(arr: IPrices[]): number[] {
        return arr.map((e) => e.price);
    }
}
