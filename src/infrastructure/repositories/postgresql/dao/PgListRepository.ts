import { DEPENDENCY_CONTAINER, TYPES } from '@configuration';
import { PostgresError } from '@domain/exceptions';
import { ListRepository } from '@domain/repositories';
import { injectable } from 'inversify';
import { PgDatabase } from '../adapter';

@injectable()
export class PgListRepository implements ListRepository {
    private db = DEPENDENCY_CONTAINER.get<PgDatabase>(TYPES.Postgres);
    async get<T>(table: string): Promise<T[]> {
        try {
            const query = `SELECT * FROM ${table}`;
            const result = await this.db.many<T>(query);
            return result;
        } catch (error) {
            throw new PostgresError(error.code, error.message);
        }
    }
}
