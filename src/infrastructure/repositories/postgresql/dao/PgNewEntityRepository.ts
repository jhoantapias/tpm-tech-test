import { DEPENDENCY_CONTAINER, TYPES } from '@configuration';
import { PostgresError } from '@domain/exceptions';
import { NewEntityRepository } from '@domain/repositories/NewEntityRepository';
import { injectable } from 'inversify';
import { PgDatabase } from '../adapter';

@injectable()
export class PgNewEntityRepository implements NewEntityRepository {
    private db = DEPENDENCY_CONTAINER.get<PgDatabase>(TYPES.Postgres);
    async create(data: Record<string, unknown>, table: string): Promise<void> {
        try {
            const query = `INSERT INTO ${table} (${'${this:name}'}) VALUES (${'${this:csv}'});`;
            await this.db.none(query, data);
        } catch (error) {
            throw new PostgresError(error.code, error.message);
        }
    }
}
