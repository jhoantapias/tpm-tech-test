import { DEPENDENCY_CONTAINER, TYPES } from '@configuration';
import { PostgresError } from '@domain/exceptions';
import { IQueryUpdate, UpdateRepository } from '@domain/repositories';
import { getUpdateSentence } from '@infrastructure/utils';
import { injectable } from 'inversify';
import { PgDatabase } from '../adapter';

@injectable()
export class PgUpdateRepository implements UpdateRepository {
    private db = DEPENDENCY_CONTAINER.get<PgDatabase>(TYPES.Postgres);
    async update(info: IQueryUpdate): Promise<void> {
        try {
            const query = getUpdateSentence(info);
            await this.db.none(query);
        } catch (error) {
            throw new PostgresError(error.code, error.message);
        }
    }
}
