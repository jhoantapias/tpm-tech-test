import { DEPENDENCY_CONTAINER, TYPES } from '@configuration';
import { readFileSync, renameSync } from 'fs';
import { injectable } from 'inversify';
import { PgDatabase } from '../../adapter';

@injectable()
export class PostgresMigrationsRepository {
    private db = DEPENDENCY_CONTAINER.get<PgDatabase>(TYPES.Postgres);

    async run(file: string): Promise<void> {
        const script = readFileSync(file).toString();
        await this.db.query(script);
        renameSync(file, file.replace('migrate', 'migrate-done'));
    }
}
