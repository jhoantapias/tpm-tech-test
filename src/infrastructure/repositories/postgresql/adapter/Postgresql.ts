import { POSTGRES_DATABASE, POSTGRES_HOST, POSTGRES_PASSWORD, POSTGRES_PORT, POSTGRES_USER } from '@util';
import dotenv from 'dotenv';
dotenv.config();
import pgPromise, { IMain, IDatabase } from 'pg-promise';
import { IConnectionParameters } from 'pg-promise/typescript/pg-subset';

export type PgDatabase = IDatabase<IMain>;

const PG_CONECTION: IConnectionParameters = {
    host: POSTGRES_HOST,
    port: POSTGRES_PORT,
    user: POSTGRES_USER,
    password: POSTGRES_PASSWORD,
    database: POSTGRES_DATABASE,
    connectionTimeoutMillis: 1000,
    max: 30,
    idleTimeoutMillis: 3000,
    query_timeout: 2500,
};
const pgp: IMain = pgPromise();
export const store = pgp(PG_CONECTION) as PgDatabase;
