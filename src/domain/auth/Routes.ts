export const ADMIN = [
    'users/new',
    'users/update',
    '/sales/update',
    'sales/delete',
    'users/delete',
    'users/list',
    'sales/report-last-month',
];
export const EMPLOYE = [];
export const EVERYONE = ['product/new', 'sales/new', 'product/list'];
