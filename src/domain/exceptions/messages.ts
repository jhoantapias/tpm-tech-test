export enum ERROR_MESSAGES {
    NO_DATA_TO_VALIDATE = 'No hay data para validar, por favor especificarla en el body ',
    ERR_INPUT_DATA = 'Los valores de entrada no son correctos.',
}
