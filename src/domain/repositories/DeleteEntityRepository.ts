export interface DeleteEntityRepository {
    delete(id: string, table: string): Promise<void>;
}
