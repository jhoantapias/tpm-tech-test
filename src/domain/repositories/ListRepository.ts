export interface ListRepository {
    get<T>(table: string): Promise<T[]>;
}
