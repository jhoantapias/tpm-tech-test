export interface NewEntityRepository {
    create<T extends abstract new (...args: never) => unknown>(data: InstanceType<T>, table: string): Promise<void>;
}
