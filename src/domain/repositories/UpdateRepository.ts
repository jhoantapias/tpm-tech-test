export interface UpdateRepository {
    update(info: IQueryUpdate): Promise<void>;
}
export interface IQueryUpdate {
    table: string;
    data: Record<string, string | number>;
    where_field: string;
    where_value: string;
}
