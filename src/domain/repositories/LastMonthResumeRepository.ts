export interface LastMonthResumeRepository {
    report(): Promise<number>;
}
