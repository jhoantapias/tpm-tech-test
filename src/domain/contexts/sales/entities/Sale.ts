import { SaleModel } from '../models';
import crypto from 'crypto';

export class SaleEntity {
    readonly id: string;
    readonly products_id: string;
    readonly qty: number;
    readonly sales_at: string;
    readonly users_id: string;

    constructor(data: SaleModel) {
        this.id = crypto.randomUUID();
        this.products_id = data.products_id;
        this.qty = data.qty;
        this.sales_at = data.sales_at;
        this.users_id = data.users_id;
    }
}
