import { SaleEntity } from '../entities';

export interface NewSaleRepository {
    create(data: SaleEntity): Promise<void>;
}
