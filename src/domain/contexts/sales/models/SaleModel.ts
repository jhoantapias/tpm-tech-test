export interface SaleModel {
    products_id: string;
    qty: number;
    sales_at: string;
    users_id: string;
    [key: string]: string | number;
}
