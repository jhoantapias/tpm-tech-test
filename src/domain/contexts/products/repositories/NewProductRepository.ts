import { ProductEntity } from '@domain/contexts';

export interface NewProductRepository {
    create(model: ProductEntity): Promise<void>;
}
