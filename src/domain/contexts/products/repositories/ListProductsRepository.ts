import { ProductEntity } from '../entities';

export interface ListProductsRepository {
    get(): Promise<ProductEntity[]>;
}
