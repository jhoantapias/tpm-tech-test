export interface ProductModel {
    description: string;
    name: string;
    price: number;
}
