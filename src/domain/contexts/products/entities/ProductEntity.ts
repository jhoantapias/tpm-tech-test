import { ProductModel } from '@domain/contexts';
import crypto from 'crypto';

export class ProductEntity implements ProductModel {
    readonly description: string;
    readonly name: string;
    readonly price: number;
    readonly id: string;

    constructor(data: ProductModel) {
        this.id = crypto.randomUUID();
        this.description = data.description;
        this.name = data.name;
        this.price = data.price;
    }
}
