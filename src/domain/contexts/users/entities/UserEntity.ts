import { UserModel } from '../models/UserModel';
import crypto from 'crypto';

export class UserEntity {
    id: string;
    document: string;
    last_name: string;
    name: string;
    roles_id: string;
    constructor(data: UserModel) {
        this.id = crypto.randomUUID();
        this.document = data.document;
        this.last_name = data.last_name;
        this.name = data.name;
        this.roles_id = data.roles_id;
    }
}
