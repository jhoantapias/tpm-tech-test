export interface UserModel {
    document: string;
    last_name: string;
    name: string;
    roles_id: string;
    [key: string]: string | number;
}
