export const TYPES = {
    Postgres: Symbol.for('Postgres'),
    ListRepository: Symbol.for('PgListRepository'),
    NewEntityRepository: Symbol.for('PgNewEntityRepository'),
    UpdateRepository: Symbol.for('PgUpdateRepository'),
    DeleteEntityRepository: Symbol.for('PgDeleteEntityRepository'),
    LastMonthResumeRepository: Symbol.for('PgLastMonthResumeRepository'),
};
