import { Container, interfaces } from 'inversify';

export class Dependency {
    constructor(private dc: Container) {}

    public service(service: interfaces.ServiceIdentifier): void {
        this.dc.bind(service).toSelf().inSingletonScope();
    }
    public repository<T>(id: symbol, constructor: new (...args: never[]) => T): void {
        this.dc.bind<T>(id).to(constructor).inSingletonScope();
    }
}
