import { UserRegisterService, UsersListService, UserUpdateService, DeleteUserService } from '@application/services/';
import { PgRoleRepository } from '@infrastructure/repositories/postgresql/dao/PgRoleRepository';
import { Container } from 'inversify';
import { Dependency } from './Dependency';

export class UserDependencies extends Dependency {
    constructor(DEPENDENCY_CONTAINER: Container) {
        super(DEPENDENCY_CONTAINER);
        super.service(UserRegisterService);
        super.service(UsersListService);
        super.service(UserUpdateService);
        super.service(DeleteUserService);
        super.service(PgRoleRepository);
    }
}
