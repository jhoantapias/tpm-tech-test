import { ProductListService, ProductRegisterService } from '@application/services';
import { Container } from 'inversify';
import { Dependency } from './Dependency';

export class ProductDependencies extends Dependency {
    constructor(DEPENDENCY_CONTAINER: Container) {
        super(DEPENDENCY_CONTAINER);
        super.service(ProductRegisterService);
        super.service(ProductListService);
    }
}
