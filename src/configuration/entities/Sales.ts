//PgNewSaleRepository implements NewSaleRepository
import {
    DeleteSaleService,
    LastMonthResumeService,
    SaleRegisterService,
    SalesListService,
    SaleUpdateService,
} from '@application/services';
import { TYPES } from '@configuration/Types';
import { PgLastMonthResumeRepository } from '@infrastructure/repositories';
import { Container } from 'inversify';
import { Dependency } from './Dependency';

export class SaleDependencies extends Dependency {
    constructor(DEPENDENCY_CONTAINER: Container) {
        super(DEPENDENCY_CONTAINER);
        super.service(SaleRegisterService);
        super.service(SalesListService);
        super.service(SaleUpdateService);
        super.service(DeleteSaleService);
        super.service(LastMonthResumeService);
        super.repository(TYPES.LastMonthResumeRepository, PgLastMonthResumeRepository);
    }
}
