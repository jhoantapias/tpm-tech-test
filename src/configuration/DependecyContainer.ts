import {
    PgDatabase,
    PgDeleteEntityRepository,
    PostgresMigrationsRepository,
    store,
    PgNewEntityRepository,
    PgUpdateRepository,
    PgListRepository,
} from '@infrastructure/repositories';
import { Container } from 'inversify';
import { ProductDependencies, SaleDependencies, UserDependencies } from './entities';
import { TYPES } from './Types';

export const DEPENDENCY_CONTAINER = new Container();

export const initDependencyContainer = (): void => {
    //IMPLEMENTATIONS
    DEPENDENCY_CONTAINER.bind<PgDatabase>(TYPES.Postgres).toConstantValue(store);

    //DB MIGRATION
    DEPENDENCY_CONTAINER.bind(PostgresMigrationsRepository).toSelf().inSingletonScope();

    //ENTITIES
    new ProductDependencies(DEPENDENCY_CONTAINER);
    new SaleDependencies(DEPENDENCY_CONTAINER);
    new UserDependencies(DEPENDENCY_CONTAINER);

    //REPOSITORIES
    DEPENDENCY_CONTAINER.bind(TYPES.ListRepository).to(PgListRepository).inSingletonScope();
    DEPENDENCY_CONTAINER.bind(TYPES.NewEntityRepository).to(PgNewEntityRepository).inSingletonScope();
    DEPENDENCY_CONTAINER.bind(TYPES.UpdateRepository).to(PgUpdateRepository).inSingletonScope();
    DEPENDENCY_CONTAINER.bind(TYPES.DeleteEntityRepository).to(PgDeleteEntityRepository).inSingletonScope();
};
