FROM node:14-alpine

RUN mkdir -p /user/src/app

WORKDIR /user/src/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn

ADD . .

RUN yarn build

EXPOSE 8080

RUN ls

CMD [ "yarn", "start" ]
