# Api test Backend

## Pasos para probar en el contenedor
- Ejecutar la los contenedores de docker-compose.
    `docker-compose up --build`

## Pasos para probar en local
- Instalar las dependencias.
    `yarn`

- Iniciar la base de datos.
    `docker-compose up postgres`

- Ejecutar la aplicación.
    `yarn dev`

- **NOTA:** Si después de probar en local se quiere ejecutar de nuevo `docker-compose up --build` hay que cambiar el nombre del archivo migrations/migrate-done.sql por migrations/migrate.sql, para que la base de datos tenga información de prueba.


## Después:  

- En consola se mostraran las rutas disponibles.
- A continuación podrá probarlas en postman o cualquier otra app que sirva para lo mismo.

### Probar las rutas: 
Lik al archivo de postman de las rutas: https://www.postman.com/collections/9a053b32149daf8550a3

