-- CREATE TABLES
CREATE TABLE public.products (
	id uuid NOT NULL,
	description text NOT NULL,
	name text NOT NULL,
	price integer NOT NULL
);
CREATE TABLE public.sales (
	id uuid NOT NULL,
	products_id uuid NOT NULL,
	qty integer NOT NULL,
	sales_at timestamp without time zone NOT NULL,
	users_id uuid NOT NULL
);
CREATE TABLE public.users (
	"document" text NOT NULL,
	id uuid NOT NULL,
	last_name text NOT NULL,
	"name" text NOT NULL,
	roles_id uuid NOT NULL
);
CREATE TABLE public.roles (
	id uuid NOT NULL,
	"name" text NOT NULL
);

-- CREATE PRIMARY KEYS
ALTER TABLE public.users ADD CONSTRAINT users_pk PRIMARY KEY (id);
ALTER TABLE public.sales ADD CONSTRAINT sales_pk PRIMARY KEY (id);
ALTER TABLE public.roles ADD CONSTRAINT roles_pk PRIMARY KEY (id);
ALTER TABLE public.products ADD CONSTRAINT products_pk PRIMARY KEY (id);

-- CREATE RELATIONS
ALTER TABLE public.sales ADD CONSTRAINT sales_fk FOREIGN KEY (products_id) REFERENCES public.products(id);
ALTER TABLE public.sales ADD CONSTRAINT sales_fk2 FOREIGN KEY (users_id) REFERENCES public.users(id);
ALTER TABLE public.users ADD CONSTRAINT users_fk FOREIGN KEY (roles_id) REFERENCES public.roles(id);

-- CREATE PRODUCTS
INSERT INTO public.products (id,description,"name",price)
	VALUES ('079fba0a-baaf-4b46-95a2-83a663817646'::uuid,'Libra','Arroz',3000);
INSERT INTO public.products (id,description,"name",price)
	VALUES ('efbff7f6-6374-4c2f-9c96-3611c65068ba'::uuid,'Libra','Papas',1000);
INSERT INTO public.products (id,description,"name",price)
	VALUES ('f7c377cf-0f92-435a-b5e6-2c8cdd9d10c6'::uuid,'500 ml','Agua sin gas',2000);
INSERT INTO public.products (id,description,"name",price)
	VALUES ('3bed5d90-64ed-4bc1-8a3a-a378737ed542'::uuid,'500 ml','Agua con gas',2500);
INSERT INTO public.products (id,description,"name",price)
	VALUES ('c3f25f98-c5c3-4a00-b550-f716ae36b25f'::uuid,'ministro haciendo aprueba','Docena de huevos',1800);

-- CREATE ROLES
INSERT INTO public.roles (id,"name")
	VALUES ('b835f04c-37b3-11ed-a261-0242ac120002'::uuid,'admin');
INSERT INTO public.roles (id,"name")
	VALUES ('fbe30c12-37b3-11ed-a261-0242ac120002'::uuid,'employee');
